-- Database creation
CREATE DATABASE `SweetTooth` /*!40100 DEFAULT CHARACTER SET utf8 */;

-- Drop table queries
DROP TABLE IF EXISTS `SweetTooth`.`restaurants`;
DROP TABLE IF EXISTS `SweetTooth`.`locations`;

-- Table locations
CREATE TABLE `SweetTooth`.`locations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));

-- Table Restaurants
CREATE TABLE `SweetTooth`.`restaurants` (
  `id` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `location_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_restaurants_1_idx` (`location_id` ASC),
  CONSTRAINT `fk_restaurants_1`
    FOREIGN KEY (`location_id`)
    REFERENCES `SweetTooth`.`locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

