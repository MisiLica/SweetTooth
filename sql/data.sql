/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Dublin'),(2,'Cork'),(3,'Navan'),(4,'Bucharest'),(5,'New York'),(6,'Vienna');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `restaurants`
--

LOCK TABLES `restaurants` WRITE;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` VALUES (1,'Littel, Considine and Cruickshank',2),(2,'Bradtke-Romaguera',2),(3,'Shields and Sons',6),(4,'Cummings LLC',2),(5,'Heller LLC',5),(6,'Langosh, Auer and Wyman',2),(7,'Goodwin-Miller',1),(8,'Hettinger Inc',1),(9,'Cruickshank-Stamm',5),(10,'Schuster, Dickinson and O\'Conner',6),(11,'Marvin-Ryan',4),(12,'Quigley-Nolan',4),(13,'Harber Inc',5),(14,'Roob-Hagenes',3),(15,'Cassin-Muller',1),(16,'Mitchell-Kihn',2),(17,'Little and Sons',4),(18,'Ward, Von and Crooks',6),(19,'Cassin, Batz and Larson',6),(20,'Kertzmann-O\'Keefe',3),(21,'O\'Reilly, Hintz and Hagenes',6),(22,'Witting-Halvorson',3),(23,'Cummerata, Stanton and Herman',2),(24,'Blick, Pacocha and Lynch',6),(25,'Kris, Zboncak and Bailey',1),(26,'Schroeder Group',5),(27,'Halvorson and Sons',4),(28,'Mueller, Kulas and Metz',1),(29,'Durgan Inc',1),(30,'Kilback Inc',5),(31,'Schowalter-Bradtke',1),(32,'Shanahan Group',5),(33,'Mueller, Schimmel and Kilback',6),(34,'Bayer LLC',6),(35,'Frami LLC',3),(36,'Bogan, Skiles and Brekke',4),(37,'Hansen Inc',5),(38,'Huels, Kling and Rolfson',5),(39,'Will and Sons',1),(40,'Botsford-Miller',3),(41,'Purdy Group',5),(42,'Waters Group',3),(43,'Weissnat-Christiansen',2),(44,'Leuschke Group',3),(45,'Hills and Sons',6),(46,'Spencer-Buckridge',6),(47,'Marvin Group',3),(48,'Kutch and Sons',2),(49,'Barton LLC',4),(50,'Abshire, Harris and Fritsch',2),(51,'Corkery Group',4),(52,'Schmidt-Schmeler',6),(53,'Abbott LLC',1),(54,'Champlin Group',6),(55,'Purdy-Funk',5),(56,'Goldner Inc',1),(57,'Abernathy Group',5),(58,'Olson Group',6),(59,'Jones Group',4),(60,'Cummerata, Skiles and Nolan',2),(61,'Shields-Stracke',5),(62,'O\'Hara-Zieme',6),(63,'Mills Group',1),(64,'McGlynn Group',2),(65,'Frami Group',4),(66,'Treutel-Gorczany',6),(67,'Schinner and Sons',4),(68,'Schaefer-Fisher',1),(69,'Purdy-Walsh',6),(70,'Roob Inc',4);
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

