<?php
define("MSG_ERROR", "Missing or unset location.");

if (! isset($_GET['location'])) {
    $errorMsg = MSG_ERROR;
} elseif (strlen(trim($_GET['location'])) == 0) {
    $errorMsg = MSG_ERROR;
} else {
    $location = $_GET['location'];
    require_once('db/DB.class.php');

    try {
        $db = DB::getInstance();
        $sql = "SELECT r.title "."FROM restaurants r LEFT JOIN locations l ON r.location_id = l.id "."WHERE l.name LIKE ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(["%".$location."%"]);
        $locationsArr = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        die(var_dump($e));
    }

    $numOfLocs = count($locationsArr);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sweettooth</title>
    <meta name="description" content="">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Glegoo" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Coda" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sigmar+One" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome-animation.min.css">

    <link rel="stylesheet" href="css/main.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/search.css">
    
    </head>

    <nav>
        <div class="header">
                <nav class="nav">
                    <ul id="nav-links">
                        <a href="index.html"><li class="logo"><img id="logo" src="img/logo.png"></li></a>
                        <li class="bars"><i class="bar fas fa-align-left fa-4x faa-parent animated-hover faa-float"></i></li>
                        
        </ul>
    </nav>
        </div>


<body>






<div class="container cardholder">
    <div class="row">

        <?php
        if (! isset($errorMsg)) {
            if ($numOfLocs == 0) { ?>
               
                <div class='col col-lg-12 col-md-12 col-sm-12 alert alert-info text-center'>Currently there are no
                    restaurants for your location (<?=$location;?>).
                </div>
                
            <?php } else {
                for ($i = 0; $i < $numOfLocs; $i++) {
                    echo "<div class='col text-center restaurants '><img src='truck.jpg' class='simple'> <h1 class='text-center mt-3 names'>{$locationsArr[$i]['title']}</h1><p class='stars text-center'>☆☆☆☆☆</p><p class='open text-center'>Currently [STATUS]</p></div>";
                }
                ?>
                <?php
            }
        } else {
            echo "<div class='col col-lg-12 col-md-12 col-sm-12 alert alert-danger text-center' role='alert'>{$errorMsg}</div>";
        }
        ?>

    </div>
</div>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/page_search.js"></script>
</body>
</html>