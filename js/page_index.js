(function(){
	var submit = function(e) {
		e.preventDefault();
		var locationText = '';
		for(var i=0;i<e.target.length;i++) {
			if(typeof e.target[i].id !== 'undefined' && e.target[i].id == 'search')
				locationText = e.target[i].value;
		}

		if(locationText.trim().length != 0) {
			window.location.href = 'search.php?location=' + locationText.trim()
		} 
	};
	document.getElementById('my-form').onsubmit = submit;
})();